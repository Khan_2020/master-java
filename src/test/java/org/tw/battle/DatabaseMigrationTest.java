package org.tw.battle;

import org.junit.jupiter.api.Test;
import org.tw.battle.dbTest.InMemoryDbSupport;

import static org.tw.battle.dbTest.TestQueryHelper.assertContainsCharacterTable;

@InMemoryDbSupport
class DatabaseMigrationTest {
    @Test
    void should_contains_character_table() throws Exception {
        assertContainsCharacterTable();
    }
}
