package org.tw.battle;

import org.flywaydb.core.Flyway;
import org.tw.battle.domain.CommandHandlerProvider;
import org.tw.battle.domain.CommandResponse;
import org.tw.battle.domain.Game;
import org.tw.battle.domain.ServiceConfiguration;

import java.io.InputStream;
import java.io.PrintStream;
import java.util.Scanner;

import static org.tw.battle.infrastructure.ArgumentParser.parse;

/**
 * @author Liu Xia
 *
 * IMPORTANT: You cannot modify this file.
 */
public class GameFacade {
    public void run(ServiceConfiguration configuration, InputStream input, PrintStream output) {
        doMigration(configuration);
        final Game game = new Game(new CommandHandlerProvider().createHandlers(configuration));
        final Scanner scanner = new Scanner(input);
        runGameLoop(output, game, scanner);
    }

    private void runGameLoop(PrintStream output, Game game, Scanner scanner) {
        while (true) {
            output.print("> ");
            final String command = scanner.nextLine();
            final String[] commandTokens = parse(command);
            final CommandResponse commandResponse = game.execute(commandTokens);
            if (commandResponse.isQuit()) {
                return;
            }

            final String message = commandResponse.getMessage();
            if (commandResponse.isSuccess()) {
                output.println(String.format("SUCCESS - %s", message == null ? "(no content)" : message));
            } else {
                output.println(String.format("ERROR - %s", message == null ? "(no content)" : message));
            }
        }
    }

    private void doMigration(ServiceConfiguration configuration) {
        Flyway migration = Flyway.configure()
            .dataSource(configuration.getUri(), configuration.getUsername(), configuration.getPassword())
            .load();
        migration.migrate();
    }
}
