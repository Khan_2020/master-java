package org.tw.battle.domain.repositories;

import org.tw.battle.domain.ServiceConfiguration;
import org.tw.battle.infrastructure.CloseConnection;
import org.tw.battle.infrastructure.DatabaseConnectionProvider;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;


import java.sql.SQLException;

/**
 * @author Liu Xia
 */
public class CharacterRepository {
    private final ServiceConfiguration configuration;

    public CharacterRepository(ServiceConfiguration configuration) {
        this.configuration = configuration;
    }

    public ServiceConfiguration getConfiguration(){
        return configuration;
    }

    public int create(String name, int hp, int x, int y) throws Exception {
        // TODO:
        //   Please implement the method.
        //   Please refer to DatabaseConnectionProvider to see how to create connection.

        Connection conn = DatabaseConnectionProvider.createConnection(configuration);
        Statement statement = null;

        try {
            statement = conn.createStatement();
        } catch (SQLException e) {
            System.out.println("The creation of statement failed");
            e.printStackTrace();
        }

        ResultSet resultSet = null;
        try {
            resultSet = statement.executeQuery(
                    "SELECT * FROM character");
        } catch (SQLException e) {
            System.out.println("query failed");
            e.printStackTrace();
        }

        int id = 0;
        while (resultSet.next()) {
            id = Math.max(id, resultSet.getInt("id"));
        }
        id++;

        try {
            PreparedStatement pst = conn.prepareStatement(
                    "INSERT INTO character VALUES(?,?,?,?,?);");
            pst.setInt(1, id);
            pst.setString(2, name);
            pst.setInt(3, hp);
            pst.setInt(4, x);
            pst.setInt(5, y);

            pst.executeUpdate();

        } catch (SQLException e) {
            System.out.println("preparedstatement failed");
            e.printStackTrace();
        }

        CloseConnection.closeConnection(resultSet, statement, conn);

        return id;

    }
}
