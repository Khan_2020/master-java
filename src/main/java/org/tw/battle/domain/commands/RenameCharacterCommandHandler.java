package org.tw.battle.domain.commands;

import org.tw.battle.domain.CommandHandler;
import org.tw.battle.domain.CommandResponse;
import org.tw.battle.domain.repositories.CharacterRepository;
import org.tw.battle.infrastructure.CloseConnection;
import org.tw.battle.infrastructure.DatabaseConnectionProvider;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class RenameCharacterCommandHandler implements CommandHandler {
    private static final String CMD_NAME = "rename-character";
    private final CharacterRepository characterRepository;

    public RenameCharacterCommandHandler(CharacterRepository characterRepository) {
        this.characterRepository = characterRepository;
    }

    @Override
    public boolean canHandle(String commandName) {
        return CMD_NAME.equals(commandName);
    }

    @Override
    public CommandResponse handle(String[] commandArgs) throws Exception {
        if(commandArgs.length!=2)
            return CommandResponse.fail("Bad command: rename-character <character id> <new name>");

        Connection conn = DatabaseConnectionProvider.createConnection(characterRepository.getConfiguration());
        Statement statement = null;

        try {
            statement = conn.createStatement();
        } catch (SQLException e) {
            System.out.println("The creation of statement failed");
            e.printStackTrace();
        }

        ResultSet resultSet = null;
        try {
            resultSet = statement.executeQuery(
                    "SELECT * FROM character WHERE id =" + commandArgs[0]);
        } catch (SQLException e) {
            System.out.println("query failed");
            e.printStackTrace();
        }
        resultSet.last();
        int resultCount = resultSet.getRow();

        if(resultCount==0){
            return CommandResponse.fail("Bad command: character not exist");
        }

        try {
            statement.executeUpdate(
                    "UPDATE character SET name = "+commandArgs[1]+" WHERE id = "+commandArgs[0]);
        } catch (SQLException e) {
            System.out.println("update failed");
            e.printStackTrace();
        }

        CloseConnection.closeConnection(resultSet, statement, conn);



        return CommandResponse.fail("Bad command: rename-character <character id> <new name>");
    }
}
