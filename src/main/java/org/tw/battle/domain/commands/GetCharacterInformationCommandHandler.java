package org.tw.battle.domain.commands;

import org.tw.battle.domain.CommandHandler;
import org.tw.battle.domain.CommandResponse;
import org.tw.battle.domain.repositories.CharacterRepository;
import org.tw.battle.infrastructure.CloseConnection;
import org.tw.battle.infrastructure.DatabaseConnectionProvider;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * @author Liu Xia
 */
public class GetCharacterInformationCommandHandler implements CommandHandler {
    // TODO: Please implement the command handler.

    private static final String CMD_NAME = "character-info";
    private final CharacterRepository characterRepository;

    public GetCharacterInformationCommandHandler(CharacterRepository characterRepository) {
        this.characterRepository = characterRepository;
    }

    @Override
    public boolean canHandle(String commandName) {
        return CMD_NAME.equals(commandName);
    }

    @Override
    public CommandResponse handle(String[] commandArgs) throws Exception {
        if(commandArgs.length==0)
            return CommandResponse.fail("Bad command: character-info <character id>");

        if(commandArgs.length>1)
            return CommandResponse.fail("Bad command: character-info <character id>");

        Connection conn = DatabaseConnectionProvider.createConnection(characterRepository.getConfiguration());
        Statement statement = null;

        try {
            statement = conn.createStatement();
        } catch (SQLException e) {
            System.out.println("The creation of statement failed");
            e.printStackTrace();
        }

        ResultSet resultSet = null;
        try {
            resultSet = statement.executeQuery(
                    "SELECT * FROM character WHERE id =" + commandArgs[0]);
        } catch (SQLException e) {
            System.out.println("query failed");
            e.printStackTrace();
        }
        resultSet.last();
        int resultCount = resultSet.getRow();

        CloseConnection.closeConnection(resultSet, statement, conn);

        if(resultCount==0){
            return CommandResponse.fail("Bad command: character not exist");
        }

        if (resultSet != null) {
            return CommandResponse.success(
                    "id: "+commandArgs[0]+", name: unnamed, hp: 100, x: 0, y: 0, status: alive");
        }

        return CommandResponse.fail("Bad command: unknown problem");

    }
}
